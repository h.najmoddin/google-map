package ir.co.ariapardaz.googlemap;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

/**
 * Created by m on 2019/06/13.
 */

public class MyApplication extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Hawk.init(this).build();
    }

    @Override
    public void onTerminate() {

        super.onTerminate();
    }


}


